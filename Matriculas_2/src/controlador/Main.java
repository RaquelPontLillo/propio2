package controlador;

import vista.VUsuario;
import vista.VPrincipal;
import modelo.IModelo;
import java.util.InputMismatchException;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Main {
    public static void main (String[] args) throws InputMismatchException {
        Configurador.getInstanciaApp("\nMATRÍCULAS APP. 2015-2016");
        
        VUsuario.validarUsuario();
        
        IModelo modelo = null;
        VPrincipal vista = new VPrincipal();
        CPrincipal controlador = new CPrincipal (modelo, vista);
    }
}