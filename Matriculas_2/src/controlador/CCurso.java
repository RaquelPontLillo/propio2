package controlador;

import java.util.HashSet;
import modelo.Curso;
import vista.VCurso;
import modelo.IModelo;
import vista.IVista;
import vista.VPrincipal;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CCurso {
    private IModelo modelo;
    private IVista<Curso> vc = new VCurso();
    private VCurso vC = new VCurso();
    
    public CCurso(IModelo m) {
        modelo = m;
        String id;
        Boolean bucle = true;
        
        do {
            char opcion = VCurso.menuCrud();
            Curso curso;

            switch (opcion) {
                case 'e':
                    VPrincipal.despedirCRUD();
                    return;
                case 'c':
                    curso = vc.alta();
                    modelo.create(curso);
                    break;
                case 'r':
                    HashSet hashset = new HashSet();
                    hashset = modelo.readCurso();
                    vC.mostrarVarios(hashset);
                    break;
                case 'u':
                    id = vC.actualizar();
                    curso = vc.alta();
                    curso.setIdCurso(id);
                    modelo.update(curso);
                    break;
                case 'd':
                    id = vC.borrar();
                    curso = new Curso(id, "", 0);
                    modelo.delete(curso);
                    break;
            }
        } while (bucle);
    }
}
