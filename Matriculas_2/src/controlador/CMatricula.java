package controlador;

import java.util.HashSet;
import modelo.Alumno;
import modelo.Curso;
import modelo.Matricula;
import vista.VMatricula;
import modelo.IModelo;
import vista.IVista;
import vista.VAlumno;
import vista.VCurso;
import vista.VPrincipal;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class CMatricula {
    private IModelo modelo = null;
    private IVista<Matricula> vm = new VMatricula();
    private IVista<Alumno> va = new VAlumno();
    private IVista<Curso> vc = new VCurso();
    private VMatricula vM = new VMatricula();
    
    public CMatricula(IModelo m) {
        modelo = m;
        Alumno alumno;
        Curso curso;
        String idM;
        String idA;
        String idC;
        Boolean bucle = true;
        
        do {
            char opcion = VMatricula.menuCrud();
            Matricula matricula;

            switch (opcion) {
                case 'e':
                    VPrincipal.despedirCRUD();
                    return;
                case 'c':
                    idA = vM.pedirIdAlumno();
                    alumno = new Alumno(idA,"","",0,"");
                    idC = vM.pedirIdCurso();
                    curso = new Curso(idC,"",0);
                    matricula = vm.alta();
                    modelo.create(matricula, alumno, curso);
                    break;
                case 'r':
                    HashSet hashset = new HashSet();
                    hashset = modelo.readMatricula();
                    vM.mostrarVarios(hashset);
                    break;
                case 'u':
                    idM = vM.actualizar();
                    matricula = vm.alta();
                    matricula.setIdMatricula(idM);
                    modelo.update(matricula);
                    break;
                case 'd':
                    idM = vM.borrar();
                    matricula = new Matricula(idM, null, null);
                    modelo.delete(matricula);
                    break;
                default:
                    VPrincipal.errorMenu();
            }
        } while (bucle);
    }
}
