package controlador;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Configurador {
    private static Configurador instancia;
    private static String nameApp;

    public static String getNombreApp() {
        return nameApp;
    }

    private Configurador(String nameApp) {
        this.nameApp = nameApp;
    }

    public static Configurador getInstanciaApp(String nameApp) {
        if (instancia == null) {
            instancia = new Configurador(nameApp);
            System.out.println("\nEjecutando aplicación, espere...");
        } else {
            System.err.println("¡Error! Únicamente se permite una instancia de la aplicación.");
        }
        return instancia;
    }
}
