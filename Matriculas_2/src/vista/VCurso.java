package vista;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import modelo.Curso;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VCurso implements IVista <Curso> {
    private static String idCurso;
    private static String nombre;
    private static int horas;

    public static char menuCrud() {
        char opt;
        System.out.println("\n***************************");
        System.out.println("* MENÚ C.R.U.D.");
        System.out.println("* -------------------------");
        System.out.println("* e. Salir (exit)");
        System.out.println("* -------------------------");
        System.out.println("* c. Crear (create)");
        System.out.println("* r. Leer (read)");
        System.out.println("* u. Actualizar (update)");
        System.out.println("* d. Borrar (delete)");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt = VPrincipal.leerCaracter();
        return opt;
    }

    public Curso alta() throws InputMismatchException {
        System.out.println("\nINTRODUCIR DATOS");
        System.out.println("Introduce el código del curso: ");
        idCurso = VPrincipal.leerTexto();
        System.out.println("Introduce el nombre del curso: ");
        nombre = VPrincipal.leerTexto();
        Boolean error = true;
        do {
            try {
                System.out.println("Introduce el número de horas del curso: ");
                horas = VPrincipal.leerNumero();
                error = false;
            } catch (InputMismatchException e) {
                VPrincipal.errorNum();
          }
        } while (error);

        Curso curso = new Curso (idCurso, nombre, horas);
        System.out.println("\nCurso dado de alta con éxito");
        return curso;
    }
  
    public void mostrar(Curso curso) {
        System.out.println("*  DATOS DEL CURSO");
        System.out.println("* ------------------------------------------------------------------------------");
        System.out.println("* Código del curso: " + curso.getIdCurso());
        System.out.println("* Título del curso: " + curso.getNombre());
        System.out.println("* Número de horas del curso: " + curso.getHoras());
 }
 
    public void mostrarVarios(HashSet hashset) {
        IVista<Curso> vc = new VCurso();
        Iterator iterator = hashset.iterator();
        Curso curso = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            curso = (Curso) iterator.next();
            vc.mostrar(curso);
            System.out.println("********************************************************************************");
        }
    }
        
    public String actualizar() {
        System.out.println("\nEscribe el código del curso que quieres modificar: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
    
    public String borrar() {
        System.out.println("\nEscribe el código del curso que quieres borrar: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
}
