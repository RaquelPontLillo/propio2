package vista;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import modelo.Alumno;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VAlumno implements IVista <Alumno> {
    private int edad;
    private String email;
    private String idPersona;
    private String nombre;
    private String dni;

    public static char menuCrud() {
        char opt;
        System.out.println("\n***************************");
        System.out.println("* MENÚ C.R.U.D.");
        System.out.println("* -------------------------");
        System.out.println("* e. Salir (exit)");
        System.out.println("* -------------------------");
        System.out.println("* c. Crear (create)");
        System.out.println("* r. Leer (read)");
        System.out.println("* u. Actualizar (update)");
        System.out.println("* d. Borrar (delete)");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt = VPrincipal.leerCaracter();
        return opt;
    }
    
    public Alumno alta() throws InputMismatchException {
        System.out.println("\nINTRODUCIR DATOS");
        System.out.println("Introduce el código del alumno: ");
        idPersona = VPrincipal.leerTexto();
        System.out.println("Introduce el nombre del alumno: ");
        nombre = VPrincipal.leerTexto();
        System.out.println("Introduce el DNI del alumno: ");
        dni = VPrincipal.leerTexto();
        
        Boolean error = true;
        Boolean valido = true;
        
        do {
            System.out.println("Introduce el e-mail del alumno: ");
            email = VPrincipal.leerTexto();
            valido = VPrincipal.validarEmail(email);
            if (valido) {
                error = false;
            } else {
                System.err.println("El e-mail introducido no es válido.");
            }
        } while (error);
        
        error = true;

        do {
            try {
                System.out.println("Introduce la edad del alumno: ");
                edad = VPrincipal.leerNumero();
                error = false;
            } catch (InputMismatchException e) {
                VPrincipal.errorNum();
          }
        } while (error);

        Alumno alumno = new Alumno (idPersona, nombre, dni, edad, email);
        System.out.println("\nAlumno dado de alta con éxito");
        return alumno;
  }
   
    public void mostrar(Alumno alumno) {
        System.out.println("* DATOS PERSONALES DEL ALUMNO");
        System.out.println("* ------------------------------------------------------------------------------");
        System.out.println("* Código personal: " + alumno.getId());
        System.out.println("* Nombre: " + alumno.getNombre() + "      DNI: " + alumno.getDni() + "      Edad: " + alumno.getEdad() + " años");
        System.out.println("* Correo electrónico: " + alumno.getEmail());
   }
    
    public void mostrarVarios(HashSet hashset) {
        IVista<Alumno> va = new VAlumno();
        Iterator iterator = hashset.iterator();
        Alumno alumno = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            alumno = (Alumno) iterator.next();
            va.mostrar(alumno);
            System.out.println("********************************************************************************");
        }
    }
        
    public String actualizar() {
        System.out.println("\nEscribe el código del alumno que quieres modificar: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
    
    public String borrar() {
        System.out.println("\nEscribe el código del alumno que quieres borrar: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
}
