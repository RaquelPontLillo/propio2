package vista;

import modelo.Usuario;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class VUsuario {
    private static String usuario = Usuario.getUsuario();
    private static String password = Usuario.getPassword();
    
  public static void validarUsuario() {
    String pass;
    String user;
    
    do {
       System.out.println("\nIntroduce el nombre de usuario (root):");
       user = VPrincipal.leerTexto();
       System.out.println("Introduce la contraseña de acceso (pass):");
       pass = VPrincipal.leerTexto();
       if (!user.equals(usuario)) {
         System.err.println("El nombre de usuario no es correcto.");
       }
       if (!pass.equals(password)) {
        System.err.println("La contraseña introducida no es correcta.");
       }
    } while (!pass.equals(password) || !user.equals(usuario)); 
  }
}
