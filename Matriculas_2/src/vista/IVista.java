package vista;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public interface IVista <T> {
    public T alta();
    public void mostrar(T t);
}
