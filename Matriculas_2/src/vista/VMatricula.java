package vista;

import modelo.Matricula;
import modelo.Alumno;
import modelo.Curso;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class VMatricula implements IVista<Matricula> {
  private String idMatricula;
  private Alumno alumno;
  private Curso curso;
 
    public static char menuCrud() {
        char opt;
        System.out.println("\n***************************");
        System.out.println("* MENÚ C.R.U.D.");
        System.out.println("* -------------------------");
        System.out.println("* e. Salir (exit)");
        System.out.println("* -------------------------");
        System.out.println("* c. Crear (create)");
        System.out.println("* r. Leer (read)");
        System.out.println("* u. Actualizar (update)");
        System.out.println("* d. Borrar (delete)");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt = VPrincipal.leerCaracter();
        return opt;
    }
    
    public Matricula alta() {
        System.out.println("\nINTRODUCIR DATOS");
        System.out.println("Introduce el código la matrícula: ");
        idMatricula = VPrincipal.leerTexto();

        Matricula matricula = new Matricula (idMatricula);
        System.out.println("\nMatrícula dada de alta con éxito");
        return matricula;
    }
  
    public void mostrar(Matricula matricula) {
        VAlumno va = new VAlumno();
        VCurso vc = new VCurso();
        System.out.println("\n********************************************************************************");
        System.out.println("*  JUSTIFICANTE DE MATRÍCULA ");
        System.out.println("* ------------------------------------------------------------------------------ ");
        System.out.println("* Código de matrícula: " + matricula.getIdMatricula());
        System.out.println("*");
        va.mostrar(matricula.getAlumno());
        System.out.println("*");
        vc.mostrar(matricula.getCurso());
        System.out.println("********************************************************************************");
        Date fecha = new Date();
        System.out.println("\nJustificante generado el: " + fecha + " || SIN VALIDEZ ACADÉMICA");
        System.out.println("\n--------------------------------------------------------------------------------");
    }

    public void mostrarVarios(HashSet hashset) {
        IVista<Matricula> vm = new VMatricula();
        Iterator iterator = hashset.iterator();
        Matricula matricula = null;

        while (iterator.hasNext()) {
            matricula = (Matricula) iterator.next();
            vm.mostrar(matricula);
            System.out.println("\n\n");
        }
    }
        
    public String actualizar() {
        System.out.println("\nEscribe el código de la matrícula que quieres modificar: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
    
    public String borrar() {
        System.out.println("\nEscribe el código de la matrícula que quieres borrar: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
    
    public String pedirIdAlumno() {
        System.out.println("\nEscribe el código del alumno que quieres matricular: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
    
    public String pedirIdCurso() {
        System.out.println("\nEscribe el código del curso en el que quieres matricularlo: ");
        String id = VPrincipal.leerTexto();
        return id;
    }
}