package modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class Usuario extends Persona {
  
  //Variables de la clase
  private static String usuario = "root";
  private static String password = "pass";
  
  //Metodos de la clase
  public static String getUsuario () {
    return usuario;
  }
  
  public static String getPassword () {
    return password;
  }
}
