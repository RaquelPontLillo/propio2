package modelo;

import java.util.HashSet;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloArray implements IModelo {
    private Alumno alumnos[] = new Alumno[99];
    private Curso cursos[] = new Curso[99];
    private Matricula matriculas[] = new Matricula[99];
    //private Alumno alumnoVacio = new Alumno("","","",0,"");
    //private Curso cursoVacio = new Curso("","",0);
    //private Matricula matriculaVacia = new Matricula("",null,null);
    int idA = 0;
    int idC = 0;
    int idM = 0;
    
    public ModeloArray() {
        for (int i = 0; i < alumnos.length; i++) {
            alumnos[i] = new Alumno("","","",0,"");
        }
        
        for (int i = 0; i < cursos.length; i++) {
            cursos[i] = new Curso("","",0);
        }
        
        for (int i = 0; i < matriculas.length; i++) {
            matriculas[i] = new Matricula("",null,null);
        }       
    }
    
    @Override
    public void create(Alumno alumno) {
        alumnos[idA] = alumno;
        idA++;
    }
    
    @Override
    public void create(Curso curso) {
        cursos[idC] = curso;
        idC++;
    }
    
    @Override
    public void create(Matricula matricula, Alumno alumno, Curso curso) {
        for (int i = 0; i < alumnos.length || i < cursos.length ; i++) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumno = alumnos[i];
            } 
            if (cursos[i].getIdCurso().equals(curso.getIdCurso())) {
                curso = cursos[i];
            }
        }
        matricula.setAlumno(alumno);
        matricula.setCurso(curso);
        matriculas[idM] = matricula;
        idM++;
    }
    
    @Override
    public HashSet readAlumno() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < alumnos.length; i++) {
            if ( !alumnos[i].getId().equals("")) {
                hashset.add(alumnos[i]);
            }
        }
        return hashset;
    }
    
    @Override
    public HashSet readCurso() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < cursos.length; i++) {
            if ( !cursos[i].getIdCurso().equals("")) {
                hashset.add(cursos[i]);
            }
        }
        return hashset;
    }
    
    @Override
    public HashSet readMatricula() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < matriculas.length; i++) {
            if ( !matriculas[i].getIdMatricula().equals("")) {
                hashset.add(matriculas[i]);
            }
        }
        return hashset;
    }
    
    @Override
    public void update(Alumno alumno) {
        for (int i = 0; i < alumnos.length; i++) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = alumno;
            }
        }
    }
    
    @Override
    public void update(Curso curso) {
        for (int i = 0; i < cursos.length; i++) {
            if (cursos[i].getIdCurso().equals(curso.getIdCurso())) {
                cursos[i] = curso;
            }
        }
    }
    
    @Override
    public void update(Matricula matricula) {
        for (int i = 0; i < matriculas.length; i++) {
            if (matriculas[i].getIdMatricula().equals(matricula.getIdMatricula())) {
                matriculas[i] = matricula;
            }
        }
    }
    
    @Override
    public void delete(Alumno alumno) {
        for (int i = 0; i < alumnos.length; i++) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = new Alumno("","","",0,"");
            }
        }
    }
    
    @Override
    public void delete(Curso curso) {
        for (int i = 0; i < cursos.length; i++) {
            if (cursos[i].getIdCurso().equals(curso.getIdCurso())) {
                cursos[i] = new Curso("","",0);
            }
        }
    }
    
    @Override
    public void delete(Matricula matricula) {
        for (int i = 0; i < matriculas.length; i++) {
            if (matriculas[i].getIdMatricula().equals(matricula.getIdMatricula())) {
                matriculas[i] = new Matricula("",null,null);
            }
        }
    }
}