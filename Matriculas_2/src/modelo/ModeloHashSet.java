package modelo;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */

public class ModeloHashSet implements IModelo {
    private HashSet<Alumno> alumnos = new HashSet();
    private HashSet<Curso> cursos = new HashSet(); 
    private HashSet<Matricula> matriculas = new HashSet();
    int idA = 0;
    int idC = 0;
    int idM = 0;

    @Override
    public void create(Alumno alumno) {
        alumnos.add(alumno);
        idA++;
    }
    
    @Override
    public void create(Curso curso) {
        cursos.add(curso);
        idC++;
    }
    
    @Override
    public void create(Matricula matricula, Alumno alumno, Curso curso) {
        matriculas.add(matricula);
        idM++;
    }

    @Override
    public HashSet readAlumno() {
        return alumnos;
    }

    @Override
    public HashSet readCurso() {
        return cursos;
    }
    
    @Override
    public HashSet readMatricula() {
        return matriculas;
    }
    
    @Override
    public void update(Alumno alumno) {
        Iterator iterator = alumnos.iterator();
        Alumno a;
        while (iterator.hasNext()) {
            a = (Alumno) iterator.next();
            if (a.getId().equals(alumno.getId())) {
                iterator.remove();
            }
        } alumnos.add(alumno);
    }
    
    @Override
    public void update(Curso curso) {
        Iterator iterator = cursos.iterator();
        Curso c;
        while (iterator.hasNext()) {
            c = (Curso) iterator.next();
            if (c.getIdCurso().equals(curso.getIdCurso())) {
                iterator.remove();
            }
        } cursos.add(curso);
    }

    @Override
    public void update(Matricula matricula) {
        Matricula toRemove = null;
        for(Matricula m: matriculas) {
            if (m.getIdMatricula().equals(matricula.getIdMatricula())) {
                toRemove = m;
            }
        }
        /*
        matriculas.remove(toRemove);
        Iterator iterator = matriculas.iterator();
        while (iterator.hasNext()) {
            m = (Matricula) iterator.next();
            if (m.getIdMatricula().equals(matricula.getIdMatricula())) {
                iterator.remove();
            }
        }
        */
        matriculas.add(matricula);
    }

    @Override
    public void delete(Alumno alumno) {
        Iterator iterator = alumnos.iterator();
        Alumno a;
        for (iterator = alumnos.iterator(); iterator.hasNext();) {
            a = (Alumno) iterator.next();
            if (a.getId().equals(alumno.getId())) {
                iterator.remove();
            }
        }
    }
    
    @Override
    public void delete(Curso curso) {
        Iterator iterator = cursos.iterator();
        Curso c;
        for (iterator = cursos.iterator(); iterator.hasNext();) {
            c = (Curso) iterator.next();
            if (c.getIdCurso().equals(curso.getIdCurso())) {
                iterator.remove();
            }
        }
    }
    
    @Override
    public void delete(Matricula matricula) {
        Iterator iterator = matriculas.iterator();
        Matricula m;
        for (iterator = matriculas.iterator(); iterator.hasNext();) {
            m = (Matricula) iterator.next();
            if (m.getIdMatricula().equals(matricula.getIdMatricula())) {
                iterator.remove();
            }
        }
    }
}
