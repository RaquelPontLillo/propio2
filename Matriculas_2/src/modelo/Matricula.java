package modelo;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class Matricula {
  //Variables de la clase
  private String idMatricula;
  private Curso Curso;
  private Alumno Alumno;
  
  //Constructores de la clase
  
  public Matricula (String idM) {
    idMatricula = idM;
  }
  
  public Matricula() {
    idMatricula = "0";
  }
  
  public Matricula(String idM, Curso cur, Alumno al) {
      idMatricula = idM;
      Curso = cur;
      Alumno = al;
  }
  
  //Métodos de la clase
 
  //Getters
 public String getIdMatricula() {
 return idMatricula;
 }
 
 public Curso getCurso() {
 return Curso;
 }
 
 public Alumno getAlumno() {
 return Alumno;
 }
 
 //Setters
 public void setIdMatricula (String idM){
 idMatricula = idM;
 }
 
 public void setCurso (Curso idC){
 Curso = idC;
 }
 
 public void setAlumno (Alumno idA){
 Alumno = idA;
 }
}
