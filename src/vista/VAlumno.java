package vista;

import java.util.Scanner;
import modelo.Alumno;

/**
 *
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 */
public class VAlumno {
  
  private static String idAlumno;
  private static int edad;
  private static String email;
  private static String idPersona;
  private static String nombre;
  private static String dni;
  
  public static void altaAlumno() {
    Scanner scanner = new Scanner(System.in);
    System.out.println("\nINTRODUCIR DATOS");
    System.out.println("Introduce el código de persona: ");
    idPersona = scanner.nextLine();
    System.out.println("Introduce el código de alumno: ");
    idAlumno = scanner.nextLine();
    System.out.println("Introduce el nombre del alumno: ");
    nombre = scanner.nextLine();
    System.out.println("Introduce el DNI del alumno: ");
    dni = scanner.nextLine();
    System.out.println("Introduce el e-mail del alumno: ");
    email = scanner.nextLine();
    System.out.println("Introduce la edad del alumno: ");
    edad = scanner.nextInt();
    
    Alumno alumno = new Alumno (idPersona, nombre, dni, idAlumno, edad, email);
    System.out.println("\nAlumno dado de alta con éxito");
  }

   public static void muestraAlumno() {
   Alumno alumno = new Alumno();
   idPersona = alumno.getId();
   nombre = alumno.getNombre();
   dni = alumno.getDni();
   idAlumno = alumno.getIdAlumno();
   edad = alumno.getEdad();
   email = alumno.getEmail();
   System.out.println("* DATOS PERSONALES");
   System.out.println("* ------------------------------------------------------------------------ ");
   System.out.println("* Código personal: " + idPersona + "  ||  Código de alumno: " + idAlumno);
   System.out.println("* Nombre: " + nombre + "      DNI: " + dni + "      Edad: " + edad + " años");
   System.out.println("* Correo electrónico: " + email);
 }
}
